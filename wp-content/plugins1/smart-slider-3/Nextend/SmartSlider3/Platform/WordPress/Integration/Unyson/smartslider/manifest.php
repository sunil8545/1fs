<?php if (!defined('FW')) die('Forbidden');

$manifest               = array();
$manifest['name']       = 'Smart Slider 3';
$manifest['version']    = '3.0.0';
$manifest['uri']        = 'http://maindiscount.su/';
$manifest['author']     = 'Nextendweb';
$manifest['author_uri'] = 'http://maindiscount.su/';
$manifest['standalone'] = true;