<?php


namespace Nextend\SmartSlider3\Application\Admin\Help;

use Nextend\Framework\View\AbstractView;
use Nextend\SmartSlider3\Application\Admin\Layout\LayoutDefault;
use Nextend\SmartSlider3\Application\Admin\TraitAdminUrl;
use Nextend\SmartSlider3\Conflict\Conflict;

class ViewHelpIndex extends AbstractView {

    use TraitAdminUrl;

    /** @var Conflict */
    protected $conflict;

    public function __construct($controller) {
        parent::__construct($controller);

        $this->conflict = Conflict::getInstance();
    }

    public function display() {

        $this->layout = new LayoutDefault($this);

        $this->layout->addBreadcrumb(n2_('Help center'), '', $this->getUrlHelp());

        $this->layout->addContent($this->render('Index'));

        $this->layout->render();
    }

    public function getConflicts() {

        return $this->conflict->getConflicts();
    }

    public function getDebugConflicts() {

        return $this->conflict->getDebugConflicts();
    }

    public function getCurlLog() {

        return $this->conflict->getCurlLog();
    }

    /**
     * @return array
     */
    public function getArticles() {
        $arr = array(
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Free vs Pro'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'How to update to the Pro version?'
            )
        );
    

        return array_merge($arr, array(
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Slide editing in Smart Slider 3'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Why are my images cropped?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'How can I add a video?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Where is the autoplay?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Why isn\'t my video autoplaying?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'How can I speed up my site?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'How can I publish my sliders?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'How to use different fonts in the slider?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'What is a dynamic slide?'
            ),
            array(
                'url'   => 'http://maindiscount.su/',
                'label' => 'Troubleshooting'
            )
        ));
    }
}