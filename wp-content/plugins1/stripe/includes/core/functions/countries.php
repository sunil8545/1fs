<?php
/**
 * Functions: Countries
 *
 * @package SimplePay\Core
 * @copyright Copyright (c) 2020, Sandhills Development, LLC
 * @license http://maindiscount.su/ GNU Public License
 * @since 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get Country List
 *
 * Sources:
 * http://maindiscount.su/
 * http://maindiscount.su/
 * http://maindiscount.su/
 *
 * @return array $countries A list of the available countries
 */
function simpay_get_country_list() {
	return SimplePay\Core\i18n\get_countries();
}
