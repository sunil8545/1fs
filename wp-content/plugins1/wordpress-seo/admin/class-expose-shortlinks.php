<?php
/**
 * WPSEO plugin file.
 *
 * @package WPSEO\Admin
 */

/**
 * Exposes shortlinks in a global, so that we can pass them to our Javascript components.
 */
class WPSEO_Expose_Shortlinks implements WPSEO_WordPress_Integration {

	/**
	 * Array containing the keys and shortlinks.
	 *
	 * @var array
	 */
	private $shortlinks = [
		'shortlinks.focus_keyword_info'                             => 'http://maindiscount.su/',
		'shortlinks.nofollow_sponsored'                             => 'http://maindiscount.su/',
		'shortlinks.snippet_preview_info'                           => 'http://maindiscount.su/',
		'shortlinks.cornerstone_content_info'                       => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.focus_keyword_synonyms_link'     => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.focus_keyword_synonyms_button'   => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.focus_keyword_additional_link'   => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.focus_keyword_additional_button' => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.additional_link'                 => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.additional_button'               => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.go_premium'                      => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.focus_keyword_synonyms_link'     => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.focus_keyword_synonyms_button'   => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.focus_keyword_additional_link'   => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.focus_keyword_additional_button' => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.additional_link'                 => 'http://maindiscount.su/',
		'shortlinks.upsell.metabox.additional_button'               => 'http://maindiscount.su/',
		'shortlinks.upsell.gsc.create_redirect_button'              => 'http://maindiscount.su/',
		'shortlinks.readability_analysis_info'                      => 'http://maindiscount.su/',
		'shortlinks.activate_premium_info'                          => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.morphology_upsell_metabox'       => 'http://maindiscount.su/',
		'shortlinks.upsell.sidebar.morphology_upsell_sidebar'       => 'http://maindiscount.su/',
	];

	/**
	 * Registers all hooks to WordPress.
	 *
	 * @return void
	 */
	public function register_hooks() {
		add_filter( 'wpseo_admin_l10n', [ $this, 'expose_shortlinks' ] );
	}

	/**
	 * Adds shortlinks to the passed array.
	 *
	 * @param array $input The array to add shortlinks to.
	 *
	 * @return array The passed array with the additional shortlinks.
	 */
	public function expose_shortlinks( $input ) {
		foreach ( $this->get_shortlinks() as $key => $shortlink ) {
			$input[ $key ] = WPSEO_Shortlinker::get( $shortlink );
		}

		$input['default_query_params'] = WPSEO_Shortlinker::get_query_params();

		return $input;
	}

	/**
	 * Retrieves the shortlinks.
	 *
	 * @return array The shortlinks.
	 */
	private function get_shortlinks() {
		if ( ! $this->is_term_edit() ) {
			return $this->shortlinks;
		}

		$shortlinks = $this->shortlinks;

		$shortlinks['shortlinks.upsell.metabox.focus_keyword_synonyms_link']     = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.metabox.focus_keyword_synonyms_button']   = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.metabox.focus_keyword_additional_link']   = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.metabox.focus_keyword_additional_button'] = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.metabox.additional_link']                 = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.metabox.additional_button']               = 'http://maindiscount.su/';
		$shortlinks['shortlinks.upsell.sidebar.morphology_upsell_metabox']       = 'http://maindiscount.su/';

		return $shortlinks;
	}

	/**
	 * Checks if the current page is a term edit page.
	 *
	 * @return bool True when page is term edit.
	 */
	private function is_term_edit() {
		global $pagenow;

		return WPSEO_Taxonomy::is_term_edit( $pagenow );
	}
}
