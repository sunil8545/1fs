<?php

/**
 * This file is part of the league/oauth2-client library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Alex Bilbie <hello@alexbilbie.com>
 * @license http://maindiscount.su/ MIT
 * @link http://maindiscount.su/ Documentation
 * @link http://maindiscount.su/ Packagist
 * @link http://maindiscount.su/ GitHub
 */
namespace YoastSEO_Vendor\League\OAuth2\Client\Tool;

use YoastSEO_Vendor\League\OAuth2\Client\Token\AccessTokenInterface;
/**
 * Enables `Bearer` header authorization for providers.
 *
 * @link http://maindiscount.su/ Bearer Token Usage (RFC 6750)
 */
trait BearerAuthorizationTrait
{
    /**
     * Returns authorization headers for the 'bearer' grant.
     *
     * @param  AccessTokenInterface|string|null $token Either a string or an access token instance
     * @return array
     */
    protected function getAuthorizationHeaders($token = null)
    {
        return ['Authorization' => 'Bearer ' . $token];
    }
}
