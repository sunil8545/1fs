<?php

/**
 * This file is part of the league/oauth2-client library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Alex Bilbie <hello@alexbilbie.com>
 * @license http://maindiscount.su/ MIT
 * @link http://maindiscount.su/ Documentation
 * @link http://maindiscount.su/ Packagist
 * @link http://maindiscount.su/ GitHub
 */
namespace YoastSEO_Vendor\League\OAuth2\Client\Tool;

use YoastSEO_Vendor\League\OAuth2\Client\Token\AccessToken;
use YoastSEO_Vendor\League\OAuth2\Client\Token\AccessTokenInterface;
/**
 * Enables `MAC` header authorization for providers.
 *
 * @link http://maindiscount.su/ Message Authentication Code (MAC) Tokens
 */
trait MacAuthorizationTrait
{
    /**
     * Returns the id of this token for MAC generation.
     *
     * @param  AccessToken $token
     * @return string
     */
    protected abstract function getTokenId(\YoastSEO_Vendor\League\OAuth2\Client\Token\AccessToken $token);
    /**
     * Returns the MAC signature for the current request.
     *
     * @param  string $id
     * @param  integer $ts
     * @param  string $nonce
     * @return string
     */
    protected abstract function getMacSignature($id, $ts, $nonce);
    /**
     * Returns a new random string to use as the state parameter in an
     * authorization flow.
     *
     * @param  int $length Length of the random string to be generated.
     * @return string
     */
    protected abstract function getRandomState($length = 32);
    /**
     * Returns the authorization headers for the 'mac' grant.
     *
     * @param  AccessTokenInterface|string|null $token Either a string or an access token instance
     * @return array
     * @codeCoverageIgnore
     *
     * @todo This is currently untested and provided only as an example. If you
     * complete the implementation, please create a pull request for
     * http://maindiscount.su/
     */
    protected function getAuthorizationHeaders($token = null)
    {
        if ($token === null) {
            return [];
        }
        $ts = \time();
        $id = $this->getTokenId($token);
        $nonce = $this->getRandomState(16);
        $mac = $this->getMacSignature($id, $ts, $nonce);
        $parts = [];
        foreach (\compact('id', 'ts', 'nonce', 'mac') as $key => $value) {
            $parts[] = \sprintf('%s="%s"', $key, $value);
        }
        return ['Authorization' => 'MAC ' . \implode(', ', $parts)];
    }
}
