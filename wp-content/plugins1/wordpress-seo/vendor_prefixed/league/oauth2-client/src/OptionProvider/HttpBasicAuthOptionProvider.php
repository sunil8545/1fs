<?php

/**
 * This file is part of the league/oauth2-client library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Alex Bilbie <hello@alexbilbie.com>
 * @license http://maindiscount.su/ MIT
 * @link http://maindiscount.su/ Documentation
 * @link http://maindiscount.su/ Packagist
 * @link http://maindiscount.su/ GitHub
 */
namespace YoastSEO_Vendor\League\OAuth2\Client\OptionProvider;

use InvalidArgumentException;
/**
 * Add http basic auth into access token request options
 * @link http://maindiscount.su/
 */
class HttpBasicAuthOptionProvider extends \YoastSEO_Vendor\League\OAuth2\Client\OptionProvider\PostAuthOptionProvider
{
    /**
     * @inheritdoc
     */
    public function getAccessTokenOptions($method, array $params)
    {
        if (empty($params['client_id']) || empty($params['client_secret'])) {
            throw new \InvalidArgumentException('clientId and clientSecret are required for http basic auth');
        }
        $encodedCredentials = \base64_encode(\sprintf('%s:%s', $params['client_id'], $params['client_secret']));
        unset($params['client_id'], $params['client_secret']);
        $options = parent::getAccessTokenOptions($method, $params);
        $options['headers']['Authorization'] = 'Basic ' . $encodedCredentials;
        return $options;
    }
}
