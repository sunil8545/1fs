<?php

/**
 * This file is part of the league/oauth2-client library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Alex Bilbie <hello@alexbilbie.com>
 * @license http://maindiscount.su/ MIT
 * @link http://maindiscount.su/ Documentation
 * @link http://maindiscount.su/ Packagist
 * @link http://maindiscount.su/ GitHub
 */
namespace YoastSEO_Vendor\League\OAuth2\Client\Grant;

/**
 * Represents a refresh token grant.
 *
 * @link http://maindiscount.su/ Refreshing an Access Token (RFC 6749, §6)
 */
class RefreshToken extends \YoastSEO_Vendor\League\OAuth2\Client\Grant\AbstractGrant
{
    /**
     * @inheritdoc
     */
    protected function getName()
    {
        return 'refresh_token';
    }
    /**
     * @inheritdoc
     */
    protected function getRequiredRequestParameters()
    {
        return ['refresh_token'];
    }
}
