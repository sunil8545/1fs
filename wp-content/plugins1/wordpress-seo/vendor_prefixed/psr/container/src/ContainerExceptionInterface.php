<?php

/**
 * @license http://maindiscount.su/ MIT (see the LICENSE file)
 */
namespace YoastSEO_Vendor\Psr\Container;

/**
 * Base interface representing a generic exception in a container.
 */
interface ContainerExceptionInterface
{
}
