<?php

/**
 * @license http://maindiscount.su/ MIT (see the LICENSE file)
 */
namespace YoastSEO_Vendor\Psr\Container;

/**
 * No entry was found in the container.
 */
interface NotFoundExceptionInterface extends \YoastSEO_Vendor\Psr\Container\ContainerExceptionInterface
{
}
