<?php

namespace MailerLiteApi\Common;

class ApiConstants {
    const BASE_URL = "http://maindiscount.su/";
    const VERSION = "v2";
    const SDK_USER_AGENT = "MailerLite PHP SDK";
    const SDK_VERSION = "2.0";
}