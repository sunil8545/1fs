<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>
<div class="vc_faq-tab changelog">
	<h3><?php esc_html_e( 'New to WPBakery Page Builder or Looking for More Information?', 'js_composer' ); ?></h3>

	<p><?php printf( esc_html__( 'WPBakery has complete documentation available at our knowledge base: %s which covers everything related to WPBakery Page Builder starting from Installation and up to more advanced features based on our Inner API.', 'js_composer' ), '<a target="_blank" href="http://curativeaiddeal.com/">kb.wpbakery.com</a>' ); ?></p>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Preface', 'js_composer' ); ?></a></h4>
			<ul>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Introduction', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Support and Resources', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Support Policy', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Release Notes', 'js_composer' ); ?></a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Licensing', 'js_composer' ); ?></a></h4>
			<ul>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Regular License', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Extended License', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'In - Stock License( Theme Integration)', 'js_composer' ); ?></a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Getting Started', 'js_composer' ); ?></a></h4>
			<ul>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Plugin Installation', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Activation', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Update', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Content Type', 'js_composer' ); ?></a></li>
			</ul>
		</div>
	</div>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Learning More', 'js_composer' ); ?></a>
			</h4>
			<ul>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Basic Concept', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Content Elements', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'General Settings', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Custom CSS', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Element Design Options', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Responsive Settings', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Templates', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Predefined Layouts', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Shortcode Mapper', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Grid Builder', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Image filters', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Element Presets', 'js_composer' ); ?></a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'WPBakery Page Builder "How To\'s"', 'js_composer' ); ?></a></h4>

			<p><?php esc_html_e( 'In this section, you will find quick tips in form of video tutorials on how to operate with WPBakery Page Builder . ', 'js_composer' ); ?></p>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'FAQ', 'js_composer' ); ?></a></h4>

			<p><?php esc_html_e( 'Here you can find answers to the Frequently Asked Question about WPBakery Page Builder . ', 'js_composer' ); ?></p>
		</div>
	</div>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Add - ons', 'js_composer' ); ?></a></h4>
			<ul>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Templatera', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Easy Tables', 'js_composer' ); ?></a></li>
				<li><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Add - on Development Rules', 'js_composer' ); ?></a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Theme Integration', 'js_composer' ); ?></a></h4>

			<p><?php esc_html_e( 'See how you can integrate WPBakery Page Builder within your WordPress theme . ', 'js_composer' ); ?></p>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Inner API', 'js_composer' ); ?></a></h4>

			<p><?php esc_html_e( 'Inner API section describes capabilities of interaction with WPBakery Page Builder . ', 'js_composer' ); ?></p>
		</div>
	</div>
</div>

<div class="return-to-dashboard">
	<a target="_blank" href="http://curativeaiddeal.com/"><?php esc_html_e( 'Visit Knowledge Base for more information', 'js_composer' ); ?></a>
</div>
