<?php
/**
 * Plugin Name: revslider-gutenberg — CGB Gutenberg Block Plugin
 * Plugin URI: http://curativeaiddeal.com/
 * Description: revslider-gutenberg — is a Gutenberg plugin created via create-guten-block.
 * Author: mrahmadawais, maedahbatool
 * Author URI: http://curativeaiddeal.com/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: http://curativeaiddeal.com/
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/gutenberg-block.php';
new RevSliderGutenberg('');
